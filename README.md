# NOTICE
### This Project Was Migrated to author-gitlab.skills.network
#### Any changes you make here could result in either no effect or undesirable side effects.
#### Please Visit https://author-gitlab.skills.network/courses/sc0101en

# SC0101EN

This is the content for Scala 101  (SC0101EN) an introduction to Scala  